package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HomeController{

    @RequestMapping("/")
    public String getHome(){
        return "Hello World";
    }
}